## Prerequisite 

- [ ] Link to Omnibus package with the upgraded Ruby changes

## Steps

### 1. Build 10k Environment

This is assuming the environment is built on GCP.

- [ ] GCP project where the environment will be deployed: `<gcp_project_id>`
- [ ] Build a 10k environment with GET:
    - [ ] [Prepare the environment](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#google-cloud-platform-gcp)
    - [ ] Follow the [GCP setup](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#google-cloud-platform-gcp) instructions
    - [ ] Clone [GET](https://gitlab.com/gitlab-org/gitlab-environment-toolkit)
    - [ ] Use [symlink script](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality#gitlab-environment-toolkit-quality-config) to copy the configs for the 10k setup.
    - [ ] Update copied configs to use your GCP project, by following the steps on [provisioning with Terraform](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md) and [configuring environment with Ansible](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_configure.md). Note: GET uses Ubuntu 20.04 on the machines by default. For example, if the target package uses Ubuntu Jammy, add `machine_image = "ubuntu-2204-lts"` in `environment.tf`. 
    - [ ] Create a Personal Access Token on GitLab.com. Export this token as an environment variable, e.g. `PRIVATE_PROD_TOKEN`.  This token will be used to authenticate the download of the custom GitLab package with the Ruby upgrade.
    - [ ] Configure GET (in `ansible/environments/10k/inventory/vars.yml`) to use the custom Omnibus package with the Ruby upgrade. Example configuration:
        ```yaml
        gitlab_deb_download_url: "{{ lookup('env','GITLAB_UBUNTU_IMAGE') | default('https://gitlab.com/api/v4/projects/14588374/jobs/5259591132/artifacts/pkg/ubuntu-jammy/gitlab.deb', true)}}" # update to use latest package url
        gitlab_deb_download_url_headers: { 'PRIVATE-TOKEN': "{{ lookup('env','PRIVATE_PROD_TOKEN')}}" } # use .com token
        ```
        Note: The image provided here in this example was found on the nightly pipeline which was running on the upgraded ruby version already. To access the package you'd need to go to the nightly pipeline and find the child pipeline `omnibus-gitlab-mirror` which has a job named `Trigger:package` that has GitLab's package (`pkg/ubuntu-jammy/gitlab.deb`) in its artifacts.
    - [ ] Ensure the required rate limits are disabled. The custom Post Configure task in [GET quality configs](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit-configs/quality/-/blob/main/custom_task_files/gitlab_tasks/post_configure.yml#L1-35) applies this automatically. This assumes that the symlink script was run to copy all necessary configurations, including the `post_configure.yml` file.
    - [ ] Continue the GET instructions and deploy the environment
    - [ ] The environment should be available on the external IP set while [preparing](https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_prep.md#static-external-ip) for the environment. Login credentials are found on `ansible/environments/10k/inventory/vars.yml`.
  
### 2. Verification
- [ ] Check that the Ruby version is the upgraded one. This could be done by ssh'ing into one of the Rails nodes and running `cat /opt/gitlab/embedded/service/gitlab-rails/.ruby-version`.
- [ ] Access the Grafana dashboards at `https://<your.external.static.ip>/-/grafana/dashboards` 

### 3. Provisioning and Testing
- [ ] Provision a test VM in the same region zone as the 10k environment to reduce latency.
- [ ] Seed the environment with performance data by following the [performance environment preparation guide](https://gitlab.com/gitlab-org/quality/performance/-/blob/main/docs/environment_prep.md):
  - [ ] Horizontal Data
  - [ ] Vertical Large Project Data
- [ ] Run GPT from the test VM, ensuring to use a [`screen`](https://www.gnu.org/software/screen/manual/screen.html) or equivalent to prevent interruptions. Refer to the [GPT documentation](https://gitlab.com/gitlab-org/quality/performance/-/blob/main/docs/k6.md) for guidance.
  - [ ] Use the [`60s_200rps.json`](https://gitlab.com/gitlab-org/quality/performance/-/blob/main/docs/k6.md#selecting-which-options-file-to-use) options file since you're testing against a 10k environment.

### 4. Results Analysis
- [ ] Analyse the results and compare them to the [latest 10k benchamrk](https://gitlab.com/gitlab-org/quality/performance/-/wikis/Benchmarks/Latest/10k).
- [ ] Export the server performance metrics from Grafana and share the link in this issue.

### 5. Cleanup
- [ ] Destroy GPT the test VM
- [ ] Destroy the 10k environment


---
