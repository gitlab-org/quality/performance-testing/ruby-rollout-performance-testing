# Ruby Rollout Performance Testing

## Getting started

This project holds the template issue to guide the performance testing of Ruby upgrades.

## Previous Upgrades

Access issues on this project for an historical archive of previous upgrades.

## Support

Find support for Ruby upgrade performance testing in `#gitlab-environment-kit` for GET specific questions and for GPT related questions use `#gitlab_performance_tool`. Also reach out to `#quality` for any additional queries.